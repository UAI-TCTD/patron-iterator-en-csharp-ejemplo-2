﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IteratorV1
{
    class Program
    {
        static void Main(string[] args)
        {

     

            var palabra = new Palabra("hola mundo");


            foreach (var item in palabra)
            {
                Console.WriteLine(item);
            }

            Console.ReadKey();
        }


    }


    

    public class Palabra : IEnumerable
    {
        string _palabra;

        public Palabra(string palabra)
        {
            _palabra = palabra;
        }

        public IEnumerator GetEnumerator()
        {
            return new PalabraIterator(_palabra);
        }
    }

    public class PalabraIterator : IEnumerator
    {
        char[] _palabra;
        int _pos;

        public PalabraIterator(string palabra)
        {
            _palabra = palabra.ToCharArray();
        }

        public object Current => _palabra[_pos];

        public bool MoveNext()
        {
            if (_pos < _palabra.Length - 1)
            {
                _pos++;
                return true;
            }
            else
            {
                return false;
            }

        }

        public void Reset()
        {
            _pos = -1;
        }
    }

}
